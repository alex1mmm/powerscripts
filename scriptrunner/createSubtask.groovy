import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue

def constantManager = ComponentAccessor.getConstantsManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getUser()
def issueFactory = ComponentAccessor.getIssueFactory()
def subTaskManager = ComponentAccessor.getSubTaskManager()
def issueManager = ComponentAccessor.getIssueManager()

Issue parentIssue = issue.getParentObject()




    MutableIssue newSubTask = issueFactory.getIssue()
    newSubTask.setAssigneeId(parentIssue.assigneeId)
    newSubTask.setSummary("test")
    newSubTask.setParentObject(parentIssue)
    newSubTask.setProjectObject(parentIssue.getProjectObject())
    newSubTask.setIssueTypeId(constantManager.getAllIssueTypeObjects().find{
        it.getName() == "Sub-task"
    }.id)
    // Add any other fields you want for the newly created sub task

    def newIssueParams = ["issue" : newSubTask] as Map<String,Object>
 
	//for JIRA v6.*
    issueManager.createIssueObject(user.directoryUser, newIssueParams)
    subTaskManager.createSubTaskIssueLink(parentIssue, newSubTask, user.directoryUser)
	// for JIRA v7.*
	issueManager.createIssueObject(user, newIssueParams)
    subTaskManager.createSubTaskIssueLink(parentIssue, newSubTask, user)

    log.info "Issue with summary ${newSubTask.summary} created"
